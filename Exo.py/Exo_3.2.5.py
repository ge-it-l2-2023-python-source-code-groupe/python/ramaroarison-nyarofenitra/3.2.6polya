  perc_GC=((4500+2575)/14800)*100
>>> print(f"le pourcentage perc_GC:{perc_GC:.0f}")
le pourcentage perc_GC:48
>>> print(f"le pourcentage perc_GC:{perc_GC:.1f}")
le pourcentage perc_GC:47.8
>>> print(f"le pourcentage perc_GC:{perc_GC:.2f}")
le pourcentage perc_GC:47.80
>>> print(f"le pourcentage perc_GC:{perc_GC:.3f}")
le pourcentage perc_GC:47.804